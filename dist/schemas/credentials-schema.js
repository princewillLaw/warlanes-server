"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CredentialsSchema = void 0;
exports.CredentialsSchema = {
    type: 'object',
    required: ['email', 'password'],
    properties: {
        email: {
            type: 'string',
            format: 'email',
        },
        password: {
            type: 'string',
            minLength: 8,
        },
    },
};
//# sourceMappingURL=credentials-schema.js.map