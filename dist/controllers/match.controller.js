"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatchController = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
const authentication_jwt_1 = require("@loopback/authentication-jwt");
const authorization_1 = require("@loopback/authorization");
let MatchController = class MatchController {
    constructor(userRepository, matchRepository) {
        this.userRepository = userRepository;
        this.matchRepository = matchRepository;
    }
    async create(match) {
        const new_match = await this.matchRepository.create(match);
        const playerOne = await this.userRepository.findById(new_match.playerOneId);
        const playerTwo = await this.userRepository.findById(new_match.playerTwoId);
        const winsOne = playerOne.id === new_match.victorId ? playerOne.wins + 1 : playerOne.wins;
        const lossesOne = playerOne.id !== new_match.victorId ? playerOne.losses + 1 : playerOne.losses;
        let pointsOne = playerOne.points + (playerOne.id === new_match.victorId ? 15 : -15);
        let rankOne = playerOne.rank;
        if (pointsOne >= 100) {
            rankOne += 1;
            pointsOne = 0;
        }
        if (pointsOne < 0) {
            if (rankOne === 1) {
                pointsOne = 0;
            }
            else {
                rankOne -= 1;
                pointsOne = 85;
            }
        }
        await this.userRepository.updateById(playerOne.id, {
            wins: winsOne,
            losses: lossesOne,
            points: !new_match.isRank ? playerOne.points : pointsOne,
            rank: !new_match.isRank ? playerOne.rank : rankOne
        });
        const winsTwo = playerTwo.id === new_match.victorId ? playerTwo.wins + 1 : playerTwo.wins;
        const lossesTwo = playerTwo.id !== new_match.victorId ? playerTwo.losses + 1 : playerTwo.losses;
        let pointsTwo = playerTwo.points + (playerTwo.id === new_match.victorId ? 15 : -15);
        let rankTwo = playerTwo.rank;
        if (pointsTwo >= 100) {
            rankTwo += 1;
            pointsTwo = 0;
        }
        if (pointsTwo < 0) {
            if (rankTwo === 1) {
                pointsTwo = 0;
            }
            else {
                rankTwo -= 1;
                pointsTwo = 85;
            }
        }
        await this.userRepository.updateById(playerTwo.id, {
            wins: winsTwo,
            losses: lossesTwo,
            points: !new_match.isRank ? playerTwo.points : pointsTwo,
            rank: !new_match.isRank ? playerTwo.rank : rankTwo
        });
        return new_match;
    }
    async count(where) {
        return this.matchRepository.count(where);
    }
    async find(filter) {
        return this.matchRepository.find(filter);
    }
    async updateAll(match, where) {
        return this.matchRepository.updateAll(match, where);
    }
    async findById(id, filter) {
        return this.matchRepository.findById(id, filter);
    }
    async findMatches(id, filter) {
        return this.matchRepository.find({ ...filter, where: { or: [{ playerOneId: id }, { playerTwoId: id }] } });
    }
    async updateById(id, match) {
        await this.matchRepository.updateById(id, match);
    }
    async replaceById(id, match) {
        await this.matchRepository.replaceById(id, match);
    }
    async deleteById(id) {
        await this.matchRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    (0, authorization_1.authorize)({ allowedRoles: ['admin', '$owner'] }),
    (0, rest_1.post)('/matches'),
    (0, rest_1.response)(200, {
        description: 'Match model instance',
        content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Match) } },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Match, {
                    title: 'NewMatch',
                    exclude: ['id'],
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "create", null);
tslib_1.__decorate([
    (0, rest_1.get)('/matches/count'),
    (0, rest_1.response)(200, {
        description: 'Match model count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.param.where(models_1.Match)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "count", null);
tslib_1.__decorate([
    (0, rest_1.get)('/matches'),
    (0, rest_1.response)(200, {
        description: 'Array of Match model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: (0, rest_1.getModelSchemaRef)(models_1.Match, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Match)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "find", null);
tslib_1.__decorate([
    (0, rest_1.patch)('/matches'),
    (0, rest_1.response)(200, {
        description: 'Match PATCH success count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Match, { partial: true }),
            },
        },
    })),
    tslib_1.__param(1, rest_1.param.where(models_1.Match)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.Match, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "updateAll", null);
tslib_1.__decorate([
    (0, authorization_1.authorize)({ allowedRoles: ['admin', '$owner'] }),
    (0, rest_1.get)('/matches/{id}'),
    (0, rest_1.response)(200, {
        description: 'Match model instance',
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Match, { includeRelations: true }),
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, rest_1.param.filter(models_1.Match, { exclude: 'where' })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "findById", null);
tslib_1.__decorate([
    (0, authorization_1.authorize)({ allowedRoles: ['admin', '$owner'] }),
    (0, rest_1.get)('/users/{id}/matches'),
    (0, rest_1.response)(200, {
        description: 'Array of user Match model instance',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: (0, rest_1.getModelSchemaRef)(models_1.Match, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, rest_1.param.filter(models_1.Match, { exclude: 'where' })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "findMatches", null);
tslib_1.__decorate([
    (0, authorization_1.authorize)({ allowedRoles: ['admin', '$owner'] }),
    (0, rest_1.patch)('/matches/{id}'),
    (0, rest_1.response)(204, {
        description: 'Match PATCH success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Match, { partial: true }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, models_1.Match]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "updateById", null);
tslib_1.__decorate([
    (0, rest_1.put)('/matches/{id}'),
    (0, rest_1.response)(204, {
        description: 'Match PUT success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, models_1.Match]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "replaceById", null);
tslib_1.__decorate([
    (0, rest_1.del)('/matches/{id}'),
    (0, rest_1.response)(204, {
        description: 'Match DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], MatchController.prototype, "deleteById", null);
MatchController = tslib_1.__decorate([
    (0, authentication_1.authenticate)('jwt'),
    (0, authorization_1.authorize)({ allowedRoles: ['admin'] }),
    tslib_1.__param(0, (0, repository_1.repository)(authentication_jwt_1.UserRepository)),
    tslib_1.__param(1, (0, repository_1.repository)(repositories_1.MatchRepository)),
    tslib_1.__metadata("design:paramtypes", [authentication_jwt_1.UserRepository,
        repositories_1.MatchRepository])
], MatchController);
exports.MatchController = MatchController;
//# sourceMappingURL=match.controller.js.map