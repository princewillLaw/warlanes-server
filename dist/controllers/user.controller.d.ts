import { TokenService } from '@loopback/authentication';
import { Credentials, MyUserService } from '@loopback/authentication-jwt';
import { UserProfile } from '@loopback/security';
import { NewUserRequest, User } from '../models';
import { UserRepository } from '../repositories';
export declare class UserController {
    jwtService: TokenService;
    userService: MyUserService;
    user: UserProfile;
    protected userRepository: UserRepository;
    constructor(jwtService: TokenService, userService: MyUserService, user: UserProfile, userRepository: UserRepository);
    signUp(newUserRequest: Omit<NewUserRequest, 'id'>): Promise<User>;
    login(credentials: Credentials): Promise<{
        token: string;
    }>;
    me(currentUserProfile: UserProfile): Promise<User>;
    updateProfile(loggedInUser: UserProfile, user: object): Promise<object>;
}
