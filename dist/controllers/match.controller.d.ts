import { Count, Filter, FilterExcludingWhere, Where } from '@loopback/repository';
import { Match } from '../models';
import { MatchRepository } from '../repositories';
import { UserRepository } from '@loopback/authentication-jwt';
export declare class MatchController {
    userRepository: UserRepository;
    matchRepository: MatchRepository;
    constructor(userRepository: UserRepository, matchRepository: MatchRepository);
    create(match: Omit<Match, 'id'>): Promise<Match>;
    count(where?: Where<Match>): Promise<Count>;
    find(filter?: Filter<Match>): Promise<Match[]>;
    updateAll(match: Match, where?: Where<Match>): Promise<Count>;
    findById(id: string, filter?: FilterExcludingWhere<Match>): Promise<Match>;
    findMatches(id: string, filter?: FilterExcludingWhere<Match>): Promise<Match[]>;
    updateById(id: string, match: Match): Promise<void>;
    replaceById(id: string, match: Match): Promise<void>;
    deleteById(id: string): Promise<void>;
}
