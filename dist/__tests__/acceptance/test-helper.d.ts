import { Client } from '@loopback/testlab';
import { WarlanesApplication } from '../..';
export declare function setupApplication(): Promise<AppWithClient>;
export interface AppWithClient {
    app: WarlanesApplication;
    client: Client;
}
