"use strict";
/* eslint-disable @typescript-eslint/ban-ts-comment */
// Copyright RehomeGhana Ltd 2022 2022. All Rights Reserved.
// Node module: rehome-api
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.MyAuthorizationProvider = void 0;
const tslib_1 = require("tslib");
const authorization_1 = require("@loopback/authorization");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const security_1 = require("@loopback/security");
const repositories_1 = require("../repositories");
let MyAuthorizationProvider = class MyAuthorizationProvider {
    constructor(user, userRepository, matchRepository, req) {
        this.userRepository = userRepository;
        this.matchRepository = matchRepository;
        this.req = req;
    }
    /**
     * @returns authenticateFn
     */
    value() {
        return this.authorize.bind(this);
    }
    async authorize(authorizationCtx, metadata) {
        var _a, _b, _c, _d, _e, _f;
        for (const arg of authorizationCtx.invocationContext.args) {
            if (typeof arg === 'object' &&
                JSON.stringify(arg).includes('credentials')) {
                return authorization_1.AuthorizationDecision.DENY;
            }
        }
        let currentUser;
        if (authorizationCtx.principals.length > 0) {
            currentUser = await this.userRepository.findById(authorizationCtx.principals[0].id);
        }
        else {
            return authorization_1.AuthorizationDecision.DENY;
        }
        // Deny if no user was gotten
        if (!currentUser) {
            return authorization_1.AuthorizationDecision.DENY;
        }
        // Authorize everything that does not have a allowedRoles property
        if (!metadata.allowedRoles) {
            return authorization_1.AuthorizationDecision.ALLOW;
        }
        // check if userType or subscribed is allowed to access the data
        const roles = ['' + currentUser.role];
        // Admin accounts bypass id verification
        if (roles.includes('admin')) {
            return authorization_1.AuthorizationDecision.ALLOW;
        }
        // check if he/she is owner of content
        if (metadata.allowedRoles.includes('$owner')) {
            // check if the endpoint first args is string representing userId
            // e.g. @get('user/{userId}/XXXXXXX') returns `userId` as args[0]
            if (((_a = currentUser.id) === null || _a === void 0 ? void 0 : _a.toString()) ===
                authorizationCtx.invocationContext.args[0].toString()) {
                return authorization_1.AuthorizationDecision.ALLOW;
            }
            /**
             * Allow access to Conversations based resources if user is the owner of the conversation
             * eg. @post('/conversations/{conversationId}/XXXXX', ...) returns `conversationId` as args[0]
             */
            try {
                if (
                // @ts-ignore: ignore if the target object is missing
                ((_b = authorizationCtx.invocationContext.target) === null || _b === void 0 ? void 0 : _b.matchRepository) &&
                    // @ts-ignore: ignore if the target object is missing
                    ((_d = (_c = authorizationCtx.invocationContext.target) === null || _c === void 0 ? void 0 : _c.matchRepository) === null || _d === void 0 ? void 0 : _d.modelClass.name) === 'Match') {
                    //let's make sure that the user is the owner of the conversation they are trying to access
                    // @ts-ignore: ignore if the target object is missing
                    const matchId = authorizationCtx.invocationContext.args[0];
                    const match = await this.matchRepository.findById(matchId);
                    //confirm that the conversation belongs to the user
                    if ((match === null || match === void 0 ? void 0 : match.playerOneId.toString()) === ((_e = currentUser.id) === null || _e === void 0 ? void 0 : _e.toString()) || (match === null || match === void 0 ? void 0 : match.playerTwoId.toString()) === ((_f = currentUser.id) === null || _f === void 0 ? void 0 : _f.toString())) {
                        return authorization_1.AuthorizationDecision.ALLOW;
                    }
                }
            }
            catch (_g) {
                console.log();
            }
        }
        let roleIsAllowed = false;
        for (const role of roles) {
            if (metadata.allowedRoles.includes(role)) {
                roleIsAllowed = true;
                break;
            }
        }
        if (roleIsAllowed) {
            return authorization_1.AuthorizationDecision.ALLOW;
        }
        return authorization_1.AuthorizationDecision.DENY;
    }
};
MyAuthorizationProvider = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, (0, repository_1.repository)(repositories_1.UserRepository)),
    tslib_1.__param(2, (0, repository_1.repository)(repositories_1.MatchRepository)),
    tslib_1.__param(3, (0, core_1.inject)(rest_1.RestBindings.Http.REQUEST)),
    tslib_1.__metadata("design:paramtypes", [Object, repositories_1.UserRepository,
        repositories_1.MatchRepository, Object])
], MyAuthorizationProvider);
exports.MyAuthorizationProvider = MyAuthorizationProvider;
//# sourceMappingURL=authorization-provider.js.map