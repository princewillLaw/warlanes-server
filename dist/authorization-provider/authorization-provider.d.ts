/// <reference types="express" />
import { AuthorizationContext, AuthorizationDecision, AuthorizationMetadata, Authorizer } from '@loopback/authorization';
import { Provider } from '@loopback/core';
import { Request } from '@loopback/rest';
import { UserProfile } from '@loopback/security';
import { MatchRepository, UserRepository } from '../repositories';
export declare class MyAuthorizationProvider implements Provider<Authorizer> {
    private userRepository;
    private matchRepository;
    private req;
    constructor(user: UserProfile, userRepository: UserRepository, matchRepository: MatchRepository, req: Request);
    /**
     * @returns authenticateFn
     */
    value(): Authorizer;
    authorize(authorizationCtx: AuthorizationContext, metadata: AuthorizationMetadata): Promise<AuthorizationDecision.ALLOW | AuthorizationDecision.DENY>;
}
