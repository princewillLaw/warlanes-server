"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
tslib_1.__exportStar(require("./user.repository"), exports);
tslib_1.__exportStar(require("./user-credential.repository"), exports);
tslib_1.__exportStar(require("./match.repository"), exports);
tslib_1.__exportStar(require("./dto/timestampcrud.repository.base"), exports);
//# sourceMappingURL=index.js.map