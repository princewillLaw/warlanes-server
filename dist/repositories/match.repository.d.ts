import { Getter } from '@loopback/core';
import { BelongsToAccessor } from '@loopback/repository';
import { DefaultDataSource } from '../datasources';
import { Match, MatchRelations, User } from '../models';
import { UserRepository } from './user.repository';
import { TimestampCrudRepository } from './dto/timestampcrud.repository.base';
export declare class MatchRepository extends TimestampCrudRepository<Match, typeof Match.prototype.id, MatchRelations> {
    protected userRepositoryGetter: Getter<UserRepository>;
    readonly playerOne: BelongsToAccessor<User, typeof Match.prototype.id>;
    readonly playerTwo: BelongsToAccessor<User, typeof Match.prototype.id>;
    readonly victor: BelongsToAccessor<User, typeof Match.prototype.id>;
    constructor(dataSource: DefaultDataSource, userRepositoryGetter: Getter<UserRepository>);
}
