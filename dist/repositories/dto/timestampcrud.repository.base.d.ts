import { DataObject, DefaultCrudRepository, Entity, Where } from '@loopback/repository';
import { Count } from '@loopback/repository/src/common-types';
import { Options } from 'loopback-datasource-juggler';
import { DefaultDataSource } from '../../datasources';
export declare abstract class TimestampCrudRepository<E extends Entity & {
    createdAt?: Date;
    updatedAt?: Date;
}, ID, Relations extends object = {}> extends DefaultCrudRepository<E, ID, Relations> {
    constructor(entityClass: typeof Entity & {
        prototype: E;
    }, dataSource: DefaultDataSource);
    create(entity: DataObject<E>, options?: Options): Promise<E>;
    updateAll(data: DataObject<E>, where?: Where<E>, options?: Options): Promise<Count>;
    updateById(id: ID, data: DataObject<E>, options?: Options): Promise<void>;
    replaceById(id: ID, data: DataObject<E>, options?: Options): Promise<void>;
}
