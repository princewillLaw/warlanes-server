"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimestampCrudRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const datasources_1 = require("../../datasources");
let TimestampCrudRepository = class TimestampCrudRepository extends repository_1.DefaultCrudRepository {
    constructor(entityClass, dataSource) {
        super(entityClass, dataSource);
    }
    async create(entity, options) {
        entity.createdAt = new Date();
        entity.updatedAt = new Date();
        return super.create(entity, options);
    }
    async updateAll(data, where, options) {
        data.updatedAt = new Date();
        return super.updateAll(data, where, options);
    }
    async updateById(id, data, options) {
        data.updatedAt = new Date();
        return super.updateById(id, data, options);
    }
    async replaceById(id, data, options) {
        data.updatedAt = new Date();
        return super.replaceById(id, data, options);
    }
};
TimestampCrudRepository = tslib_1.__decorate([
    tslib_1.__param(1, (0, core_1.inject)('datasources.mongo')),
    tslib_1.__metadata("design:paramtypes", [Object, datasources_1.DefaultDataSource])
], TimestampCrudRepository);
exports.TimestampCrudRepository = TimestampCrudRepository;
//# sourceMappingURL=timestampcrud.repository.base.js.map