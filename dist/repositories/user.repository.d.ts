import { Getter } from '@loopback/core';
import { HasOneRepositoryFactory } from '@loopback/repository';
import { DefaultDataSource } from '../datasources';
import { User, UserCredential, UserRelations } from '../models';
import { UserCredentialRepository } from './user-credential.repository';
import { TimestampCrudRepository } from './dto/timestampcrud.repository.base';
export declare class UserRepository extends TimestampCrudRepository<User, typeof User.prototype.id, UserRelations> {
    protected userCredentialRepositoryGetter: Getter<UserCredentialRepository>;
    readonly userCredentials: HasOneRepositoryFactory<UserCredential, typeof User.prototype.id>;
    constructor(dataSource: DefaultDataSource, userCredentialRepositoryGetter: Getter<UserCredentialRepository>);
    findCredentials(userId: string): Promise<UserCredential>;
}
