"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
const timestampcrud_repository_base_1 = require("./dto/timestampcrud.repository.base");
let UserRepository = class UserRepository extends timestampcrud_repository_base_1.TimestampCrudRepository {
    constructor(dataSource, userCredentialRepositoryGetter) {
        super(models_1.User, dataSource);
        this.userCredentialRepositoryGetter = userCredentialRepositoryGetter;
        this.userCredentials = this.createHasOneRepositoryFactoryFor('userCredentials', userCredentialRepositoryGetter);
        this.registerInclusionResolver('userCredentials', this.userCredentials.inclusionResolver);
    }
    async findCredentials(userId) {
        return this.userCredentials(userId).get();
    }
};
UserRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.defaultdb')),
    tslib_1.__param(1, repository_1.repository.getter('UserCredentialRepository')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.DefaultDataSource, Function])
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map