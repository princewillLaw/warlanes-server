"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatchRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
const timestampcrud_repository_base_1 = require("./dto/timestampcrud.repository.base");
let MatchRepository = class MatchRepository extends timestampcrud_repository_base_1.TimestampCrudRepository {
    constructor(dataSource, userRepositoryGetter) {
        super(models_1.Match, dataSource);
        this.userRepositoryGetter = userRepositoryGetter;
        this.victor = this.createBelongsToAccessorFor('victor', userRepositoryGetter);
        this.registerInclusionResolver('victor', this.victor.inclusionResolver);
        this.playerTwo = this.createBelongsToAccessorFor('playerTwo', userRepositoryGetter);
        this.registerInclusionResolver('playerTwo', this.playerTwo.inclusionResolver);
        this.playerOne = this.createBelongsToAccessorFor('playerOne', userRepositoryGetter);
        this.registerInclusionResolver('playerOne', this.playerOne.inclusionResolver);
    }
};
MatchRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.defaultdb')),
    tslib_1.__param(1, repository_1.repository.getter('UserRepository')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.DefaultDataSource, Function])
], MatchRepository);
exports.MatchRepository = MatchRepository;
//# sourceMappingURL=match.repository.js.map