import { Entity } from '@loopback/repository';
import { USER_ROLE } from '../types/enum';
import { UserCredential } from './user-credential.model';
export declare class User extends Entity {
    id?: string;
    userName: string;
    firstName: string;
    lastName: string;
    email: string;
    role: USER_ROLE;
    wins: number;
    losses: number;
    rank: number;
    points: number;
    createdAt?: Date;
    updatedAt?: Date;
    userCredentials: UserCredential;
    constructor(data?: Partial<User>);
}
export interface UserRelations {
}
export type UserWithRelations = User & UserRelations;
