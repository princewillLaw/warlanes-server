import { Entity } from '@loopback/repository';
export declare class Match extends Entity {
    id?: string;
    gamelog: string;
    isRank: boolean;
    playerOneId: string;
    playerTwoId: string;
    victorId: string;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(data?: Partial<Match>);
}
export interface MatchRelations {
}
export type MatchWithRelations = Match & MatchRelations;
