import { Entity } from '@loopback/repository';
export declare class UserCredential extends Entity {
    password: string;
    id?: string;
    userId?: string;
    constructor(data?: Partial<UserCredential>);
}
export interface UserCredentialRelations {
}
export type UserCredentialWithRelations = UserCredential & UserCredentialRelations;
