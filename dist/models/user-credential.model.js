"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCredential = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
let UserCredential = class UserCredential extends repository_1.Entity {
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], UserCredential.prototype, "password", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        id: true,
        generated: true,
    }),
    tslib_1.__metadata("design:type", String)
], UserCredential.prototype, "id", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], UserCredential.prototype, "userId", void 0);
UserCredential = tslib_1.__decorate([
    (0, repository_1.model)(),
    tslib_1.__metadata("design:paramtypes", [Object])
], UserCredential);
exports.UserCredential = UserCredential;
//# sourceMappingURL=user-credential.model.js.map