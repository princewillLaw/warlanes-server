import {Entity, hasOne, model, property} from '@loopback/repository';
import {USER_ROLE} from '../types/enum';
import {UserCredential} from './user-credential.model';

@model()
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  userName: string;

  @property({
    type: 'string',
  })
  firstName: string;

  @property({
    type: 'string',
  })
  lastName: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: Object.values(USER_ROLE),
    },
    required: false,
    default: USER_ROLE.player,
  })
  role: USER_ROLE;

  @property({
    type: 'number',
    default: 0,
  })
  wins: number;

  @property({
    type: 'number',
    default: 0,
  })
  losses: number;

  @property({
    type: 'number',
    default: 1,
  })
  rank: number;

  @property({
    type: 'number',
    default: 0,
  })
  points: number;
  
  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @hasOne(() => UserCredential)
  userCredentials: UserCredential;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
