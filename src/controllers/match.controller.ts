import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {Match} from '../models';
import {MatchRepository} from '../repositories';
import { UserRepository } from '@loopback/authentication-jwt';
import { authorize } from '@loopback/authorization';

@authenticate('jwt')
@authorize({allowedRoles: ['admin']})
export class MatchController {
  constructor(
    @repository(UserRepository)
    public userRepository : UserRepository,
    @repository(MatchRepository)
    public matchRepository : MatchRepository,
  ) {}


  @authorize({allowedRoles: ['admin','$owner']})
  @post('/matches')
  @response(200, {
    description: 'Match model instance',
    content: {'application/json': {schema: getModelSchemaRef(Match)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Match, {
            title: 'NewMatch',
            exclude: ['id'],
          }),
        },
      },
    })
    match: Omit<Match, 'id'>,
  ): Promise<Match> {
    const new_match = await this.matchRepository.create(match);

    const playerOne = await this.userRepository.findById(new_match.playerOneId);
    const playerTwo = await this.userRepository.findById(new_match.playerTwoId);

    const winsOne = playerOne.id === new_match.victorId ? playerOne.wins+1 : playerOne.wins;
    const lossesOne = playerOne.id !== new_match.victorId ? playerOne.losses+1 : playerOne.losses;

    let pointsOne = playerOne.points + (playerOne.id === new_match.victorId ? 15 : -15);
    let rankOne = playerOne.rank;
    if(pointsOne >= 100){
      rankOne += 1;
      pointsOne = 0;
    }
    if(pointsOne < 0){
      if(rankOne === 1){
        pointsOne = 0;
      }else{
        rankOne -= 1;
        pointsOne = 85;
      }
    }

    await this.userRepository.updateById(playerOne.id,{
      wins: winsOne,
      losses: lossesOne,
      points: !new_match.isRank ? playerOne.points : pointsOne,
      rank:  !new_match.isRank ? playerOne.rank : rankOne
    })

    const winsTwo = playerTwo.id === new_match.victorId ? playerTwo.wins+1 : playerTwo.wins;
    const lossesTwo = playerTwo.id !== new_match.victorId ? playerTwo.losses+1 : playerTwo.losses;
    let pointsTwo = playerTwo.points + (playerTwo.id === new_match.victorId ? 15 : -15);
    let rankTwo = playerTwo.rank;
    if(pointsTwo >= 100){
      rankTwo += 1;
      pointsTwo = 0;
    }
    if(pointsTwo < 0){
      if(rankTwo === 1){
        pointsTwo = 0;
      }else{
        rankTwo -= 1;
        pointsTwo = 85;
      }
    }

    await this.userRepository.updateById(playerTwo.id,{
      wins: winsTwo,
      losses: lossesTwo,
      points:  !new_match.isRank ? playerTwo.points : pointsTwo,
      rank:  !new_match.isRank ? playerTwo.rank : rankTwo
    })

    return new_match
  }

  @authorize({allowedRoles: ['admin','$owner']})
  @post('/users/{id}/matches')
  @response(200, {
    description: 'Match model instance',
    content: {'application/json': {schema: getModelSchemaRef(Match)}},
  })
  async createMatch(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Match, {
            title: 'NewMatch',
            exclude: ['id'],
          }),
        },
      },
    })
    match: Omit<Match, 'id'>,
    @param.path.string('id') id: string,
  ): Promise<Match> {
    const user = await this.userRepository.findById(id);

    if(match.playerOneId.toString() !== user.id.toString()  && user.role !== 'admin'){
      throw new HttpErrors.BadRequest("Access Denied")
    }

    const new_match = await this.matchRepository.create(match);

    const playerOne = await this.userRepository.findById(new_match.playerOneId);
    const playerTwo = await this.userRepository.findById(new_match.playerTwoId);

    const winsOne = playerOne.id === new_match.victorId ? playerOne.wins+1 : playerOne.wins;
    const lossesOne = playerOne.id !== new_match.victorId ? playerOne.losses+1 : playerOne.losses;

    let pointsOne = playerOne.points + (playerOne.id === new_match.victorId ? 15 : -15);
    let rankOne = playerOne.rank;
    if(pointsOne >= 100){
      rankOne += 1;
      pointsOne = 0;
    }
    if(pointsOne < 0){
      if(rankOne === 1){
        pointsOne = 0;
      }else{
        rankOne -= 1;
        pointsOne = 85;
      }
    }

    await this.userRepository.updateById(playerOne.id,{
      wins: winsOne,
      losses: lossesOne,
      points: !new_match.isRank ? playerOne.points : pointsOne,
      rank:  !new_match.isRank ? playerOne.rank : rankOne
    })

    const winsTwo = playerTwo.id === new_match.victorId ? playerTwo.wins+1 : playerTwo.wins;
    const lossesTwo = playerTwo.id !== new_match.victorId ? playerTwo.losses+1 : playerTwo.losses;
    let pointsTwo = playerTwo.points + (playerTwo.id === new_match.victorId ? 15 : -15);
    let rankTwo = playerTwo.rank;
    if(pointsTwo >= 100){
      rankTwo += 1;
      pointsTwo = 0;
    }
    if(pointsTwo < 0){
      if(rankTwo === 1){
        pointsTwo = 0;
      }else{
        rankTwo -= 1;
        pointsTwo = 85;
      }
    }

    await this.userRepository.updateById(playerTwo.id,{
      wins: winsTwo,
      losses: lossesTwo,
      points:  !new_match.isRank ? playerTwo.points : pointsTwo,
      rank:  !new_match.isRank ? playerTwo.rank : rankTwo
    })

    return new_match
  }

  @get('/matches/count')
  @response(200, {
    description: 'Match model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Match) where?: Where<Match>,
  ): Promise<Count> {
    return this.matchRepository.count(where);
  }

  @get('/matches')
  @response(200, {
    description: 'Array of Match model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Match, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Match) filter?: Filter<Match>,
  ): Promise<Match[]> {
    return this.matchRepository.find(filter);
  }

  @patch('/matches')
  @response(200, {
    description: 'Match PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Match, {partial: true}),
        },
      },
    })
    match: Match,
    @param.where(Match) where?: Where<Match>,
  ): Promise<Count> {
    return this.matchRepository.updateAll(match, where);
  }

  
  @authorize({allowedRoles: ['admin','$owner']})
  @get('/matches/{id}')
  @response(200, {
    description: 'Match model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Match, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Match, {exclude: 'where'}) filter?: FilterExcludingWhere<Match>
  ): Promise<Match> {
    return this.matchRepository.findById(id, filter);
  }

  @authorize({allowedRoles: ['admin','$owner']})
  @get('/users/{id}/matches')
  @response(200, {
    description: 'Array of user Match model instance',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Match, {includeRelations: true}),
        },
      },
    },
  })
  async findMatches(
    @param.path.string('id') id: string,
    @param.filter(Match, {exclude: 'where'}) filter?: FilterExcludingWhere<Match>
  ): Promise<Match[]> {
    return this.matchRepository.find({...filter,where: {or: [{playerOneId: id},{playerTwoId: id}]}});
  }

  @authorize({allowedRoles: ['admin','$owner']})
  @patch('/matches/{id}')
  @response(204, {
    description: 'Match PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Match, {partial: true}),
        },
      },
    })
    match: Match,
  ): Promise<void> {
    await this.matchRepository.updateById(id, match);
  }

  @put('/matches/{id}')
  @response(204, {
    description: 'Match PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() match: Match,
  ): Promise<void> {
    await this.matchRepository.replaceById(id, match);
  }

  @del('/matches/{id}')
  @response(204, {
    description: 'Match DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.matchRepository.deleteById(id);
  }
}
