import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasOneRepositoryFactory, repository} from '@loopback/repository';
import {DefaultDataSource} from '../datasources';
import {User, UserCredential, UserRelations} from '../models';
import {UserCredentialRepository} from './user-credential.repository';
import {TimestampCrudRepository} from './dto/timestampcrud.repository.base';

export class UserRepository extends TimestampCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {

  public readonly userCredentials: HasOneRepositoryFactory<UserCredential, typeof User.prototype.id>;

  constructor(
    @inject('datasources.defaultdb') dataSource: DefaultDataSource, @repository.getter('UserCredentialRepository') protected userCredentialRepositoryGetter: Getter<UserCredentialRepository>,
  ) {
    super(User, dataSource);
    this.userCredentials = this.createHasOneRepositoryFactoryFor('userCredentials', userCredentialRepositoryGetter,);
    this.registerInclusionResolver('userCredentials', this.userCredentials.inclusionResolver);
  }

  async findCredentials(userId: string){
    return this.userCredentials(userId).get()
  }
}
