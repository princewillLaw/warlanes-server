import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DefaultDataSource} from '../datasources';
import {Match, MatchRelations, User} from '../models';
import {UserRepository} from './user.repository';
import {TimestampCrudRepository} from './dto/timestampcrud.repository.base';

export class MatchRepository extends TimestampCrudRepository<
  Match,
  typeof Match.prototype.id,
  MatchRelations
> {

  public readonly playerOne: BelongsToAccessor<User, typeof Match.prototype.id>;

  public readonly playerTwo: BelongsToAccessor<User, typeof Match.prototype.id>;

  public readonly victor: BelongsToAccessor<User, typeof Match.prototype.id>;

  constructor(
    @inject('datasources.defaultdb') dataSource: DefaultDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Match, dataSource);
    this.victor = this.createBelongsToAccessorFor('victor', userRepositoryGetter,);
    this.registerInclusionResolver('victor', this.victor.inclusionResolver);
    this.playerTwo = this.createBelongsToAccessorFor('playerTwo', userRepositoryGetter,);
    this.registerInclusionResolver('playerTwo', this.playerTwo.inclusionResolver);
    this.playerOne = this.createBelongsToAccessorFor('playerOne', userRepositoryGetter,);
    this.registerInclusionResolver('playerOne', this.playerOne.inclusionResolver);
  }
}
