/* eslint-disable @typescript-eslint/naming-convention */

export enum USER_ROLE {
  player = 'player',
  admin = 'admin'
}
