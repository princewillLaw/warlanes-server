/* eslint-disable @typescript-eslint/ban-ts-comment */
// Copyright RehomeGhana Ltd 2022 2022. All Rights Reserved.
// Node module: rehome-api
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {
  AuthorizationContext,
  AuthorizationDecision,
  AuthorizationMetadata,
  Authorizer,
} from '@loopback/authorization';
import {Provider, inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Request, RestBindings} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {User} from '../models';
import {
  MatchRepository,
  UserRepository,
} from '../repositories';

export class MyAuthorizationProvider implements Provider<Authorizer> {
  constructor(
    @inject(SecurityBindings.USER) user: UserProfile,
    @repository(UserRepository)
    private userRepository: UserRepository,
    @repository(MatchRepository)
    private matchRepository: MatchRepository,
    @inject(RestBindings.Http.REQUEST)
    private req: Request,
  ) {}

  /**
   * @returns authenticateFn
   */
  value(): Authorizer {
    return this.authorize.bind(this);
  }

  async authorize(
    authorizationCtx: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ) {
    for (const arg of authorizationCtx.invocationContext.args) {
      if (
        typeof arg === 'object' &&
        JSON.stringify(arg).includes('credentials')
      ) {
        return AuthorizationDecision.DENY;
      }
    }
    let currentUser: User;

    if (authorizationCtx.principals.length > 0) {
      currentUser = await this.userRepository.findById(
        authorizationCtx.principals[0].id
      );
    } else {
      return AuthorizationDecision.DENY;
    }

    // Deny if no user was gotten
    if (!currentUser) {
      return AuthorizationDecision.DENY;
    }

    // Authorize everything that does not have a allowedRoles property
    if (!metadata.allowedRoles) {
      return AuthorizationDecision.ALLOW;
    }

    // check if userType or subscribed is allowed to access the data
    const roles = ['' + currentUser.role];

    // Admin accounts bypass id verification
    if (roles.includes('admin')) {
      return AuthorizationDecision.ALLOW;
    }

    // check if he/she is owner of content
    if (metadata.allowedRoles.includes('$owner')) {
      // check if the endpoint first args is string representing userId
      // e.g. @get('user/{userId}/XXXXXXX') returns `userId` as args[0]
      if (
        currentUser.id?.toString() ===
        authorizationCtx.invocationContext.args[0].toString()
      ) {
        return AuthorizationDecision.ALLOW;
      }

      /**
       * Allow access to Conversations based resources if user is the owner of the conversation
       * eg. @post('/conversations/{conversationId}/XXXXX', ...) returns `conversationId` as args[0]
       */
      try {
        if (
          // @ts-ignore: ignore if the target object is missing
          authorizationCtx.invocationContext.target?.matchRepository &&
          // @ts-ignore: ignore if the target object is missing
          authorizationCtx.invocationContext.target?.matchRepository
            ?.modelClass.name === 'Match'
        ) {
          //let's make sure that the user is the owner of the conversation they are trying to access
          // @ts-ignore: ignore if the target object is missing
          const matchId = authorizationCtx.invocationContext.args[0];
          const match = await this.matchRepository.findById(
            matchId,
          );
          //confirm that the conversation belongs to the user
          if (match?.playerOneId.toString() === currentUser.id?.toString() || match?.playerTwoId.toString() === currentUser.id?.toString() ) {
            return AuthorizationDecision.ALLOW;
          }
        }
      } catch {
        console.log();
      }
    }

    let roleIsAllowed = false;
    for (const role of roles) {
      if (metadata.allowedRoles!.includes(role)) {
        roleIsAllowed = true;
        break;
      }
    }

    if (roleIsAllowed) {
      return AuthorizationDecision.ALLOW;
    }

    return AuthorizationDecision.DENY;
  }
}
